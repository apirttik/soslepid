

from DeepJetCore.TrainData import TrainData, fileTimeOut
from DeepJetCore import SimpleArray
import numpy as np

class TrainData_SOSMuonId(TrainData):
    def __init__(self):
        TrainData.__init__(self)
        # no class member is mandatory
        self.description = "This is a TrainData SOSLepId example file. Having a description string is not a bad idea (but not mandatory), e.g. for describing the array structure."
        #define any other (configuration) members that seem useful
        self.someusefulemember = "something you might need later"

        self.pdgid = 13 #set muon pdgid

        
    #def createWeighterObjects(self, allsourcefiles):
        # 
        # This function can be used to derive weights (or whatever quantity)
        # based on the entire data sample. It should return a dictionary that will then
        # be passed to either of the following functions. The weighter objects
        # should be pickleable.
        # In its default implementation, the dict is empty
        # return {}
    
    
    def convertFromSourceFile(self, filename, weighterobjects, istraining):
        # This is the only really mandatory function (unless writeFromSourceFile is defined).
        # It defines the conversion rule from an input source file to the lists of training 
        # arrays self.x, self.y, self.w
        #  self.x is a list of input feature arrays
        #  self.y is a list of truth arrays
        #  self.w is optional and can contain a weight array 
        #         (needs to have same number of entries as truth array)
        #         If no weights are needed, this can be left completely empty
        #
        # The conversion should convert finally to numpy arrays. In the future, 
        # also tensorflow tensors will be supported.
        #
        # In this example, differnt ways of reading files are deliberatly mixed
        # 
        
        
        import uproot3 as uproot

        print('reading '+filename)                

        #read friends tree to get truth info (LepGood_mcMatchId)
        friendsFilename = filename.split(".root")[0] + "_Friend.root"
        print('reading friends file ' +friendsFilename)
        friendsFile = uproot.open(friendsFilename)["Friends"]

        truth = np.expand_dims(friendsFile.array("LepGood_mcMatchId").flatten(), axis=1)
        truth = truth.astype(dtype='float32', order='C') #important, float32 and C-type!

        #read "standard" tree to get all the other LepGood variables
        eventsFile = uproot.open(filename)["Events"]

        pdgid = np.abs(eventsFile.array("LepGood_pdgId").flatten())
        
        #define "features" (variables to use for training)
        feature_array = np.concatenate([np.expand_dims(eventsFile.array("LepGood_pt").flatten(),             axis=1), 
                                        np.expand_dims(eventsFile.array("LepGood_eta").flatten(),            axis=1),
                                        np.expand_dims(eventsFile.array("LepGood_pfRelIso03_all").flatten(), axis=1),
                                        np.expand_dims(eventsFile.array("LepGood_ip3d").flatten(),           axis=1),
                                        np.expand_dims(eventsFile.array("LepGood_sip3d").flatten(),          axis=1)],
                                       axis=1)

        #select muons (or electrons in TrainData_SOSElectronId)
        feature_array = feature_array[pdgid==self.pdgid]
        truth = truth[pdgid==self.pdgid]
        
        self.nsamples=len(feature_array)

        #to apply the id for analysis, will need to put it back in the event, so will need the row_splits defined below:
        #row_splits is 0 + the cumulative sum of nLepGood
        #the network will train only on the "features", not the row_splits
        #int_array = eventsFile.array("nLepGood")
        #int_array = int_array.astype(dtype='int64', order='C')

        #need to choose int_array for the selected pdgids, but the line below doesn't work:
        #int_array = int_array[pdgid==self.pdgid]
        
        #row_splits = np.cumsum(int_array,axis=0)
        #row_splits = np.concatenate([np.array([0],dtype='int64'),row_splits],axis=0)
        
        #returns a list of feature arrays, a list of truth arrays (and optionally list of weight arrays)
        #comment out row_splits for now, until we get the int_array to work with selected pdgids
        #return [SimpleArray(feature_array,row_splits,name="features")], [SimpleArray(truth,row_splits,name="truth")], []
        return [SimpleArray(feature_array,name="features")], [SimpleArray(truth,name="truth")], []
    
    ## defines how to write out the prediction
    def writeOutPrediction(self, predicted, features, truth, weights, outfilename, inputfile):
        
        from root_numpy import array2root

	#reading the variables which are saved to output file 
        isSignal, pt, eta, pfRelIso03_all, ip3d, sip3d = truth[0][:,0:1], features[0][:,0:1], features[0][:,1:2], features[0][:,2:3], features[0][:,3:4], features[0][:,4:5]

        out = np.concatenate([predicted[0], isSignal, pt, eta, pfRelIso03_all, ip3d, sip3d],axis=-1)

        names = 'prob, isSignal, pt, eta, pfRelIso03_all, ip3d, sip3d'
        out = np.core.records.fromarrays(out.transpose(), 
                                             names=names)

        array2root(out, outfilename, 'tree')

class TrainData_SOSElectronId(TrainData_SOSMuonId):
    def __init__(self):
        TrainData_SOSMuonId.__init__(self)
        self.pdgid = 11 #set electron pdgid

