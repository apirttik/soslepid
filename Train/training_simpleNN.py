

from DeepJetCore.training.training_base import training_base
import keras
from keras.models import Model
from keras.layers import Dense, BatchNormalization #etc

from Losses import null_loss

def my_model(Inputs,otheroption):
    
    x = Inputs[0] #this is the self.x list from the TrainData data structure
    x = BatchNormalization(momentum=0.9)(x)
    x = Dense(128, activation='relu')(x)
    x = Dense(128, activation='relu')(x)
    x = Dense(128, activation='relu')(x)
    
    x = BatchNormalization(momentum=0.9)(x)
    x = Dense(128, activation='relu')(x)
    x = Dense(128, activation='relu')(x)
    
    # 1 prediction class
    x = Dense(1, activation='sigmoid')(x)
    
    predictions = [x]
    return Model(inputs=Inputs, outputs=predictions)


train=training_base(testrun=False,resumeSilently=False,renewtokens=False,splittrainandtest=0.8)

if not train.modelSet(): # allows to resume a stopped/killed training. Only sets the model if it cannot be loaded from previous snapshot

    train.setModel(my_model,otheroption=1)
    
    train.compileModel(learningrate=0.003,
                       loss='binary_crossentropy') 
                   
print(train.keras_model.summary())


model,history = train.trainModel(nepochs=30, 
                                 batchsize=5000,
                                 checkperiod=1, # saves a checkpoint model every N epochs
                                 verbose=1)
                                 
print('Since the training is done, use the predict.py script to predict the model output on your test sample, e.g.: predict.py <training output>/KERAS_model.h5 <training output>/trainsamples.djcdc <path to data>/test.txt <output dir>')
