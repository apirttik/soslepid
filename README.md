The first time you set this up:
1. git clone this package, or make a fork
2. link the input trees to this directory in this way:
```
ln -s /eos/cms/store/cmst3/group/tthlep/peruzzi/NanoTrees_SOS_070220_v6_skim_2lep_met125/2018/TTJets_SingleLeptonFromT.root ./TTJets_SingleLeptonFromT_2018.root
ln -s /eos/cms/store/cmst3/group/tthlep/peruzzi/NanoTrees_SOS_070220_v6_skim_2lep_met125/2018/TTJets_SingleLeptonFromTbar.root ./TTJets_SingleLeptonFromTbar_2018.root
ln -s /eos/cms/store/cmst3/group/tthlep/peruzzi/NanoTrees_SOS_070220_v6_skim_2lep_met125/2017/TTJets_SingleLeptonFromT.root ./TTJets_SingleLeptonFromT_2017.root
ln -s /eos/cms/store/cmst3/group/tthlep/peruzzi/NanoTrees_SOS_070220_v6_skim_2lep_met125/2017/TTJets_SingleLeptonFromTbar.root ./TTJets_SingleLeptonFromTbar_2017.root
ln -s /eos/cms/store/cmst3/group/tthlep/peruzzi/NanoTrees_SOS_070220_v6_skim_2lep_met125/2016/TTJets_SingleLeptonFromT.root ./TTJets_SingleLeptonFromT_2016.root
ln -s /eos/cms/store/cmst3/group/tthlep/peruzzi/NanoTrees_SOS_070220_v6_skim_2lep_met125/2016/TTJets_SingleLeptonFromTbar.root ./TTJets_SingleLeptonFromTbar_2016.root


ln -s /eos/cms/store/cmst3/group/tthlep/peruzzi/NanoTrees_SOS_070220_v6_skim_2lep_met125/2018/recleaner/TTJets_SingleLeptonFromT_Friend.root ./TTJets_SingleLeptonFromT_2018_Friend.root
ln -s /eos/cms/store/cmst3/group/tthlep/peruzzi/NanoTrees_SOS_070220_v6_skim_2lep_met125/2018/recleaner/TTJets_SingleLeptonFromTbar_Friend.root ./TTJets_SingleLeptonFromTbar_2018_Friend.root
ln -s /eos/cms/store/cmst3/group/tthlep/peruzzi/NanoTrees_SOS_070220_v6_skim_2lep_met125/2017/recleaner/TTJets_SingleLeptonFromT_Friend.root ./TTJets_SingleLeptonFromT_2017_Friend.root
ln -s /eos/cms/store/cmst3/group/tthlep/peruzzi/NanoTrees_SOS_070220_v6_skim_2lep_met125/2017/recleaner/TTJets_SingleLeptonFromTbar_Friend.root ./TTJets_SingleLeptonFromTbar_2017_Friend.root
ln -s /eos/cms/store/cmst3/group/tthlep/peruzzi/NanoTrees_SOS_070220_v6_skim_2lep_met125/2016/recleaner/TTJets_SingleLeptonFromT_Friend.root ./TTJets_SingleLeptonFromT_2016_Friend.root
ln -s /eos/cms/store/cmst3/group/tthlep/peruzzi/NanoTrees_SOS_070220_v6_skim_2lep_met125/2016/recleaner/TTJets_SingleLeptonFromTbar_Friend.root ./TTJets_SingleLeptonFromTbar_2016_Friend.root
```

Every time you log in and want to run something:
1. start the container with the interactive bash script provided in the DeepJetCore README
2. cd to this directory, then ```source env.sh```

To convert the root ntuples into the training data format:
```
convertFromSource.py -i inputRootFiles.txt -o TrainDataSOSMuon -c TrainData_SOSMuonId --noramcopy
convertFromSource.py -i inputRootFiles.txt -o TrainDataSOSElectron -c TrainData_SOSElectronId --noramcopy

cd TrainDataSOSMuon
mergeOrSplitFiles.py dataCollection.djcdc 10000 --randomise
mergeOrSplitFiles.py dataCollection._n.djcdc 10000 --randomise

cd ../TrainDataSOSElectron
mergeOrSplitFiles.py dataCollection.djcdc 10000 --randomise
mergeOrSplitFiles.py dataCollection._n.djcdc 10000 --randomise
cd ..
```
In the commands above: first you convert the input to the right format. Then with the ```mergeOrSplitFiles.py``` script, you first split the data, and then make sure it is randomized.

Next the test data set is separated from the training data (80% for training and 20% for testing):
```
mkdir TestDataSOSMuon
mv TrainDataSOSMuon/TTJets_SingleLeptonFromT_2016_n_0_n_{126..157}.djctd TestDataSOSMuon/

mkdir TestDataSOSElectron
mv TrainDataSOSElectron/TTJets_SingleLeptonFromT_2016_n_0_n_{83..104}.djctd TestDataSOSElectron/

```
The corresponding entries need to be removed from the ```dataCollection._n._n.djcdc``` files:

```
sed -i -E "/aVTTJets_SingleLeptonFromT_2016_n_0_n_12[6-9]|13[0-9]|14[0-9]|15[0-8].djctd/,+1d" TrainDataSOSMuon/dataCollection._n._n.djcdc

sed -i -E "/aVTTJets_SingleLeptonFromT_2016_n_0_n_8[3-9]|9[0-9]|10[0-4].djctd/,+1d" TrainDataSOSElectron/dataCollection._n._n.djcdc
```
Finally, the dataCollection files should be made for the test data:
```
cd TestDataSOSMuon
createDataCollectionFromTD.py -c TrainData_SOSMuonId TTJets_SingleLeptonFromT_2016_n_0_n_*.djctd -o testDataCollection.djcdc
cd ../TestDataSOSElectron

createDataCollectionFromTD.py -c TrainData_SOSElectronId TTJets_SingleLeptonFromT_2016_n_0_n_*.djctd -o testDataCollection.djcdc
cd ..
```
Note that the size of the test data set can be varied. In this example test data set contains 100000 leptons (10 files with 10000 lepton each).

To train the model (train over the split and randomized data):
```
python3 Train/training_simpleNN.py TrainDataSOSMuon/dataCollection._n._n.djcdc TrainOutputSOSMuon

python3 Train/training_simpleNN.py TrainDataSOSElectron/dataCollection._n._n.djcdc TrainOutputSOSElectron
```
After the training is done, the model can be used to predict the output of the model for the test data:
```
predict.py TrainOutputSOSMuon/KERAS_model.h5 TrainOutputSOSMuon/trainsamples.djcdc TestDataSOSMuon/testDataCollection.djcdc PredictSOSMuon

predict.py TrainOutputSOSElectron/KERAS_model.h5 TrainOutputSOSElectron/trainsamples.djcdc TestDataSOSElectron/testDataCollection.djcdc PredictSOSElectron
```
For some reason the ouput files created by ```predict.py``` have ```djcdc```-suffix, although they are actually ROOT files. The output files should be named accordingly and the suffixes in ```outfiles.txt``` updated:
```
cd PredictSOSMuon
rename .djctd .root *.djctd
sed -i 's/djctd/root/g' outfiles.txt
cd ../PredictSOSElectron

rename .djctd .root *.djctd
sed -i 's/djctd/root/g' outfiles.txt
cd ..
```
Note that the ```rename``` command does not work in the container but it should work generally on lxplus.  

Finally, the performance of the network can be visualized with the scripts in the directory ```plotting_scripts```. It's easiest to copy the plotting scripts to ```PredictSOSMuon``` and ```PredictSOSElectron``` directories and do the plotting there:
```
cp plotting_scripts/makeResultsPlotsMuon.py PredictSOSMuon/
cp plotting_scripts/makeResultsPlotsElectron.py PredictSOSElectron/
``` 
For some reason the plotting doesn't work if there is different amount of leptons in the output ROOT files. All the other files contain 10000 leptons expect the very last ones (index 157 for muons and 104 for electrons). Names of these files need to be removed from the ```output.txt``` files. After this, the plotting can be done with commands:
```
cd PredictSOSMuon
python3 makeResultsPlotsMuon.py outfiles.txt muonPlots
cd ../PredictSOSElectron

python3 makeResultsPlotsElectron.py outfiles.txt electronPlots
cd ..
```
